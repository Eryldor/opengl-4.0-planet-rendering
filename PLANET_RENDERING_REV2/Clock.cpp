#include "Clock.h"

#ifdef _WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN 1
#endif
#include <windows.h> // for perf timer
#else
#include <ctime> // for clock()
#endif

Clock::Clock():
m_ticks(0), m_pticks(0), m_totalTicks(0), m_seconds(0.0f), m_totalSeconds(0.0f)
{
	// get frequency/initialize current ticks
#ifdef _WIN32
	LARGE_INTEGER tmp;
	QueryPerformanceFrequency(&tmp);
	m_freq = (__int64) tmp.QuadPart;
	QueryPerformanceCounter(&tmp);
	m_cticks = (__int64) tmp.QuadPart;
#else
	m_freq = (__int64) CLOCKS_PER_SEC;
	m_cticks = (__int64) clock();
#endif

	// get 1/frequency
	m_rfreq = 1.0f / float(m_freq);
}

void Clock::Tick()
{
	m_pticks = m_cticks;

	// get new value for current ticks
#ifdef _WIN32
	LARGE_INTEGER tmp;
	QueryPerformanceCounter(&tmp);
	m_cticks = (__int64) tmp.QuadPart;
#else
	m_cticks = (__int64) clock();
#endif

	// calculate deltas
	m_ticks = m_cticks - m_pticks;
	m_totalTicks += m_ticks;
	m_seconds = float(m_ticks) * m_rfreq;
	m_totalSeconds += m_seconds;
}

void Clock::Reset()
{
	m_ticks = 0;
	m_totalTicks = 0;
	m_seconds = 0.0f;
	m_totalSeconds = 0.0f;

	// get new value for current ticks
#ifdef _WIN32
	LARGE_INTEGER tmp;
	QueryPerformanceCounter(&tmp);
	m_cticks = (__int64) tmp.QuadPart;
#else
	m_cticks = (__int64) clock();
#endif
}