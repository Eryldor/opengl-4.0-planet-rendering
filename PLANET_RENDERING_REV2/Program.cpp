#include "Program.h"

Program::Program(const std::string& path): m_id(GL_ZERO)
{
	if (!PHYSFS_exists(path.c_str())) {
		printf("Unable to find '%s'", path.c_str());
		return;
	}

	PHYSFS_file* handle = PHYSFS_openRead(path.c_str());
	size_t size = (size_t)PHYSFS_fileLength(handle);
	char* data = new char[size + 1];

	PHYSFS_read(handle, data, 1, size);
	PHYSFS_close(handle);
	data[size] = '\0';

	std::string buffer = static_cast<const char*>(data);
	std::string separator = "-- ";

	size_t h_len = buffer.find(separator, 0);
	std::string header = buffer.substr(0, h_len - 1);
	size_t start = 0;

	while ((start = buffer.find(separator, start)) != std::string::npos)
	{
		size_t len = separator.length();
		size_t k_pos = start + separator.length();
		size_t k_len = buffer.find("\n", k_pos);
		std::string key = buffer.substr(k_pos, (k_len - k_pos) - 1);
		len += key.length();

		size_t s_pos = k_len;
		size_t s_len = buffer.find(separator, s_pos);
		std::string shd = buffer.substr(s_pos, (s_len - s_pos) - 1);
		len += shd.length();

		unsigned int type = 0;
		if (key == "vs")
			type = GL_VERTEX_SHADER;
		if (key == "tcs")
			type = GL_TESS_CONTROL_SHADER;
		if (key == "tes")
			type = GL_TESS_EVALUATION_SHADER;
		if (key == "gs")
			type = GL_GEOMETRY_SHADER;
		if (key == "fs")
			type = GL_FRAGMENT_SHADER;
		if (key == "cs")
			type = GL_COMPUTE_SHADER;

		if (!type) {
			printf("%s: '%s' key is not recognized",
			path.c_str(), key.c_str());
			break;
		}

		m_stages[type] = glCreateShader(type);
		const std::string processed = header + shd;
		glShaderSource(m_stages[type], 1, (const char**)&processed, 0);
		glCompileShader(m_stages[type]);
		glVerifyState();

		int status;
		glGetShaderiv(m_stages[type], GL_COMPILE_STATUS, &status);
		if (!status)
		{
			int lenght;
			glGetShaderiv(m_stages[type], GL_INFO_LOG_LENGTH, &lenght);

			char* log = new char[lenght + 1];
			glGetShaderInfoLog(m_stages[type], lenght, 0, log);
			printf("Error: %s\n", log);
			delete[] log;
			break;
		}

		if (!m_id) {
			m_id = glCreateProgram();
			glVerifyState();
		}

		glAttachShader(m_id, m_stages[type]);
		start += len;
	}

	printf("Linking '%s' program\n", path.c_str());
	glLinkProgram(m_id);
	glVerifyState();
}

void Program::Bind() const {
	glUseProgram(m_id);
}