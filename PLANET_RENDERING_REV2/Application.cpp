#include "Application.h"

int Application::Execute(int argc, char** argv)
{
	Clock timer;
	if (Init(argc, argv)) {
		while (m_window.isOpen()) {
			timer.Tick();

			float dt = timer.GetSeconds();
			Update(dt);
			Render(dt);
		}

		Cleanup();
		return EXIT_SUCCESS;
	}

	sf::sleep(sf::seconds(1.f));
	return EXIT_FAILURE;
}

bool Application::Init(int argc, char** argv)
{
	if (!PHYSFS_init(argv[0]) ||
		!PHYSFS_addToSearchPath(PHYSFS_getBaseDir(), 1)) {
		printf("Failed PHYSFS_init(): %s\n", PHYSFS_getLastError());
		return false;
	}

	printf("Initialized PhysFS version %d.%d.%d\n", PHYSFS_VER_MAJOR,
	PHYSFS_VER_MINOR, PHYSFS_VER_PATCH);

	unsigned int e;
	if ((e = glewInit()) != GLEW_OK) {
		printf("Failed glewInit(): %s\n", glewGetErrorString(e));
		glVerifyState();
		return false;
	}

	printf("Initialized Glew version %s\n", glewGetString(GLEW_VERSION));
	printf("Detected OpenGL %s on %s %s\n", glGetString(GL_VERSION),
	glGetString(GL_VENDOR), glGetString(GL_RENDERER));

	if (!glewIsSupported("GL_VERSION_4_0")) {
		printf("OpenGL 4.0 is required\n");
		return false;
	}

	return true;
}

void Application::Update(float dt)
{
	static sf::Event event;
	while (m_window.pollEvent(event))
	{
		switch (event.type) {
		case sf::Event::Closed:
			m_window.close();
			break;

		default:
			break;
		}
	}

	ivec2 pos;
	pos.x = sf::Mouse::getPosition(m_window).x;
	pos.y = sf::Mouse::getPosition(m_window).y;

	static ivec2 prevPos = pos;
	ivec2 idelta = pos - prevPos;
	vec2 delta = vec2(
		float(idelta.x) / m_window.getSize().x,
		float(idelta.y) / m_window.getSize().y
	);

	float r = CAM_ROTATION_ACCEL * dt;
	float s = (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift)) ?
		CAM_MOVEMENT_ACCEL * 60 : CAM_MOVEMENT_ACCEL;

	if (sf::Mouse::isButtonPressed(sf::Mouse::Right)) {
		m_camera.RotateY(delta.x * r);
		m_camera.RotateX(delta.y * r);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		m_camera.Forward(s * dt);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		m_camera.Backward(s * dt);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		m_camera.Left(s * dt);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		m_camera.Right(s * dt);
	}

	m_camera.Transform(dt);
	prevPos = pos;
}

void Application::Render(float dt)
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glViewport(0, 0, m_window.getSize().x, m_window.getSize().y);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	m_planet.Draw(&m_camera, dt);
	m_window.display();
}

void Application::Cleanup()
{
}