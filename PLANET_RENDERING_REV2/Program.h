/**
* @file Program.h
* @brief Todo
*/

#pragma once

#include "Common.h"

class Program : public Referenced
{
	public:
		// CTOR/DTOR:
		Program(const std::string& path);
		virtual ~Program();

		// SERVICES:
		void Bind() const;

		// UNIFORMS (matrix):
		void UniformMat3(const std::string& name, const float* array);
		void UniformMat3(const std::string& name, const mat3& matrix);
		void UniformMat4(const std::string& name, const float* array);
		void UniformMat4(const std::string& name, const mat4& matrix);

		// UNIFORMS (others):
		void Uniform(const std::string& name, float v1, float v2, float v3, float v4);
		void Uniform(const std::string& name, float v1, float v2, float v3);
		void Uniform(const std::string& name, float v1, float v2);
		void Uniform(const std::string& name, float v);
		void Uniform(const std::string& name, int v);

		// ATTRIBUTES:
		GLuint GetAttribute(const std::string& name);

	private:
		// MEMBERS:
		std::map<GLuint, GLuint> m_stages;
		GLuint m_id;
};

////////////////////////////////////////////////////////////////////////////////
// Program inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline void Program::UniformMat3(const std::string& name, const float* array)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniformMatrix3fv(loc, 1, false, array);
}
/*----------------------------------------------------------------------------*/
inline void Program::UniformMat3(const std::string& name, const mat3& matrix) {
	UniformMat3(name, glm::value_ptr(matrix));
}
/*----------------------------------------------------------------------------*/
inline void Program::UniformMat4(const std::string& name, const float* array)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniformMatrix4fv(loc, 1, false, array);
}
/*----------------------------------------------------------------------------*/
inline void Program::UniformMat4(const std::string& name, const mat4& matrix) {
	UniformMat4(name, glm::value_ptr(matrix));
}

/*----------------------------------------------------------------------------*/
inline void Program::Uniform(const std::string& name, float v1, float v2, float v3, float v4)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniform4f(loc, v1, v2, v3, v4);
}
/*----------------------------------------------------------------------------*/
inline void Program::Uniform(const std::string& name, float v1, float v2, float v3)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniform3f(loc, v1, v2, v3);
}
/*----------------------------------------------------------------------------*/
inline void Program::Uniform(const std::string& name, float v1, float v2)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniform2f(loc, v1, v2);
}
/*----------------------------------------------------------------------------*/
inline void Program::Uniform(const std::string& name, float v)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniform1f(loc, v);
}
/*----------------------------------------------------------------------------*/
inline void Program::Uniform(const std::string& name, int v)
{
	Bind();
	GLuint loc = glGetUniformLocation(m_id, name.c_str());
	glUniform1i(loc, v);
}

/*----------------------------------------------------------------------------*/
inline GLuint Program::GetAttribute(const std::string& name)
{
	Bind();
	GLuint loc = glGetAttribLocation(m_id, name.c_str());
	return loc;
}

/*----------------------------------------------------------------------------*/
inline Program::~Program() {
	glDeleteProgram(m_id);
	m_id = GL_ZERO;
}