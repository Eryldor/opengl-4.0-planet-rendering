/**
* @file PlanetBrush.h
* @brief Todo.
*/

#pragma once

#include "Program.h"

class PlanetBrush : public Referenced
{
	public:
		// CTOR/DTOR:
		PlanetBrush(const std::string& path);
		virtual ~PlanetBrush();

		// SERVICES:
		bool Load(const std::string& path);
		void Blit(Program* prog);

	private:
		// MEMBERS:
		GLuint m_image;
};

////////////////////////////////////////////////////////////////////////////////
// PlanetBrush inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline PlanetBrush:: PlanetBrush(const std::string& path) : m_image(GL_ZERO) {
	if (!Load(path)) printf("Unable to load brush '%s'\n", path.c_str());
}
/*----------------------------------------------------------------------------*/
inline PlanetBrush::~PlanetBrush() {
	if (m_image) {
		glDeleteTextures(1, &m_image);
		m_image = GL_ZERO;
	}
}