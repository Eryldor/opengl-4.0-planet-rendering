/**
* @file Clock.h
* @brief Todo
*/

#pragma once

class Clock
{
	public:
		// CTOR:
		Clock();

		// SERVICES:
		void Tick();
		void Reset();

		// ACCESSORS:
		__int64 GetFrequency() const;
		__int64 GetTicks() const;
		__int64 GetTotalTicks() const;

		float GetSeconds() const;
		float GetTotalSeconds() const;

	private:
		// MEMBERS:
		__int64 m_ticks;
		__int64 m_cticks;
		__int64 m_pticks;
		__int64 m_totalTicks;
		__int64 m_freq;

		// 1/freq, as a real type
		float m_rfreq;

		// Seconds since previous call to Tick()
		float m_seconds;

		// Total seconds since clock initialized
		// or previous call to Reset()
		float m_totalSeconds;
};

////////////////////////////////////////////////////////////////////////////////
// Clock inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline __int64 Clock::GetFrequency() const {
	return m_freq;
}
/*----------------------------------------------------------------------------*/
inline __int64 Clock::GetTicks() const {
	return m_ticks;
}
/*----------------------------------------------------------------------------*/
inline __int64 Clock::GetTotalTicks() const {
	return m_totalTicks;
}
/*----------------------------------------------------------------------------*/
inline float Clock::GetSeconds() const {
	return m_seconds;
}
/*----------------------------------------------------------------------------*/
inline float Clock::GetTotalSeconds() const {
	return m_totalSeconds;
}