#include "PlanetBrush.h"

bool PlanetBrush::Load(const std::string& path)
{
	if (!m_image && PHYSFS_exists(path.c_str())) {
		PHYSFS_file* handle = PHYSFS_openRead(path.c_str());
		size_t size = (size_t)PHYSFS_fileLength(handle);
		char* data = new char[size + 1];

		PHYSFS_read(handle, data, 1, size);
		PHYSFS_close(handle);
		data[size] = '\0';

		glGenTextures(1, &m_image);
		glBindTexture(GL_TEXTURE_2D, m_image);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_R16, 2048, 2048, 0, GL_LUMINANCE, GL_UNSIGNED_SHORT, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		return true;
	}

	return false;
}

void PlanetBrush::Blit(Program* prog)
{
	if (m_image) {
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_image);
		prog->Uniform("s_HeightMap", 0);
	}

	static float pos[] = {
		-1,-1, 0, 1,
		 1,-1, 0, 1,
		-1, 1, 0, 1,
		 1, 1, 0, 1
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(4, GL_FLOAT, 0, pos);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glDisableClientState(GL_VERTEX_ARRAY);
}