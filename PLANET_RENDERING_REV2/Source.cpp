#include "Application.h"

int main(int argc, char** argv) {
	std::string title = "Planet Rendering - [WASD] to move / [Left Shift] to go faster / [Up/Down] to change LOD factor / [Space] to enable wireframe";
	smart_ptr<Application> app = new Application(1280, 720, title);
	return app->Execute(argc, argv);
}