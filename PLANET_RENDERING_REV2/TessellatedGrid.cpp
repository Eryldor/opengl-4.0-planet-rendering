#include "TessellatedGrid.h"

void TessellatedGrid::Draw()
{
	if (m_ENumber == 0) {
		std::vector<glm::vec4> vertices;
		vertices.resize(m_size*m_size);
		const float offsetX = 1.f / (m_size-1);
		const float offsetZ = 1.f / (m_size-1);
		for (size_t z = 0; z < m_size; z++)
		for (size_t x = 0; x < m_size; x++)
		{
			vertices[x + z*m_size].x = x*offsetX;
			vertices[x + z*m_size].y = z*offsetZ;
			vertices[x + z*m_size].z = 0;
			vertices[x + z*m_size].w = 1;
		}

		glGenBuffers(1, &m_VBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_VBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vec4) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);

		std::vector<unsigned short> indices;
		indices.resize((m_size-1)*(m_size-1) * 4);
		for (size_t z = 0; z < m_size-1; z++)
		for (size_t x = 0; x < m_size-1; x++)
		{
			int offset = (x+z * (m_size-1)) * 4;
			unsigned short p1 = x + z*m_size;
			unsigned short p2 = p1 + m_size;
			unsigned short p3 = p2 + 1;
			unsigned short p4 = p1 + 1;

			indices[offset + 0] = p1;
			indices[offset + 1] = p2;
			indices[offset + 2] = p3;
			indices[offset + 3] = p4;
		}

		glGenBuffers(1, &m_EBuffer);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned short) * indices.size(), &indices[0], GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		m_ENumber = indices.size();
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBuffer);
	glVertexPointer(4, GL_FLOAT, sizeof(vec4), (void*)0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBuffer);

	glPatchParameteri(GL_PATCH_VERTICES, 4);
	glDrawElements(GL_PATCHES, m_ENumber, GL_UNSIGNED_SHORT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glDisableClientState(GL_VERTEX_ARRAY);
}