/**
* @file Camera.h
* @brief John Chapman's quaterion based camera.
* http://john-chapman-graphics.blogspot.ca/
*/

#pragma once

#include "Common.h"

#define CAM_ROTATION_ACCEL 50.0f
#define CAM_MOVEMENT_ACCEL 15.0f

class Camera
{
	public:
		// CTOR/DTOR:
		Camera(float aspect, float fov, float near, float far);
		virtual ~Camera();

		// SERVICES:
		void Transform(float dt);

		// Apply an impulse to the
		// x/y/z rotation velocity
		void RotateX(float r);
		void RotateY(float r);
		void RotateZ(float r);

		// Apply movement impulse
		void Forward(float s);
		void Backward(float s);
		void Left(float s);
		void Right(float s);

		// ACCESSORS:
		const mat4& GetProjMatrix() const;
		const mat4& GetViewMatrix() const;
		const vec3& GetPosition() const;
		void SetPosition(const vec3& p);
		float GetAspectRatio() const;
		float GetFieldOfView() const;
		float GetNear() const;
		float GetFar() const;

	protected:
		struct CameraMatrices {
			mat4 vMatrix;
			mat4 pMatrix;
		};

		// MEMBERS:
		CameraMatrices m_current;
		float m_aspect;
		float m_fov;
		float m_near;
		float m_far;

	private:
		// speed = velocity magnitude
		float m_maxVelocitySpeed;
		float m_maxRotationSpeed;
		float m_rotationDamp;
		float m_velocityDamp;

		// MEMBERS:
		vec3 m_position;
		vec3 m_velocity;
		quat m_rotation;

		// euler in radians/sec
		vec3 m_euler;

		// represents position/rotation,
		// for getting local axes
		mat4 m_local;
};

////////////////////////////////////////////////////////////////////////////////
// Camera inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline void Camera::RotateX(float r) {
	m_euler.x -= r;
}
/*----------------------------------------------------------------------------*/
inline void Camera::RotateY(float r) {
	m_euler.y -= r;
}
/*----------------------------------------------------------------------------*/
inline void Camera::RotateZ(float r) {
	m_euler.z -= r;
}

/*----------------------------------------------------------------------------*/
inline void Camera::Forward(float s) {
	m_velocity -= glm::vec3(m_local[2]) * s;
}
/*----------------------------------------------------------------------------*/
inline void Camera::Backward(float s) {
	Forward(-s);
}
/*----------------------------------------------------------------------------*/
inline void Camera::Left(float s) {
	Right(-s);
}
/*----------------------------------------------------------------------------*/
inline void Camera::Right(float s) {
	m_velocity += glm::vec3(m_local[0]) * s;
}

/*----------------------------------------------------------------------------*/
inline const mat4& Camera::GetProjMatrix() const {
	return m_current.pMatrix;
}
/*----------------------------------------------------------------------------*/
inline const mat4& Camera::GetViewMatrix() const {
	return m_current.vMatrix;
}
/*----------------------------------------------------------------------------*/
inline const vec3& Camera::GetPosition() const {
	return m_position;
}
/*----------------------------------------------------------------------------*/
inline void Camera::SetPosition(const vec3& p) {
	m_position = p;
	Transform(0.0);
}

/*----------------------------------------------------------------------------*/
inline Camera::Camera(float aspect, float fov, float near, float far) :
m_aspect(aspect), m_fov(fov), m_near(near), m_far(far)
{
	m_current.vMatrix = mat4(1);
	m_current.pMatrix = mat4(1);

	m_maxVelocitySpeed = 250.0f;
	m_maxRotationSpeed = 10.0f;
	m_rotationDamp = 0.000005f;
	m_velocityDamp = 0.001f;

	m_position = vec3(0, 0, 0);
	m_velocity = vec3(0, 0, 0);
	m_rotation = quat();

	m_euler = vec3(0, 0, 0);
	m_local = mat4(1);

	// update the matrices when
	// creating the camera
	Transform(0);
}
/*----------------------------------------------------------------------------*/
inline Camera::~Camera() {
}