/**
* @file Application.h
* @brief Todo
*/

#pragma once

#include "Common.h"
#include "Camera.h"
#include "Planet.h"

class Application : public Referenced
{
	public:
		// CTOR/DTOR:
		Application(int w, int h, const std::string& title);
		virtual ~Application();

		// SERVICES:
		int Execute(int argc, char** argv);

	protected:
		// VIRTUALS:
		virtual bool Init(int argc, char** argv);
		virtual void Update(float dt);
		virtual void Render(float dt);
		virtual void Cleanup();

	private:
		// MEMBERS:
		sf::Window m_window;
		Camera m_camera;
		Planet m_planet;
};

////////////////////////////////////////////////////////////////////////////////
// Application inline implementation:
////////////////////////////////////////////////////////////////////////////////
/*----------------------------------------------------------------------------*/
inline Application::Application(int w, int h, const std::string& title):
m_camera(float(w)/h, glm::radians(60.0f), 0.1f, 5000.0f),
m_planet(1500.0f, 150.0f)
{
	// create render window
	m_window.create(sf::VideoMode(w, h, 32), title, sf::Style::Fullscreen, sf::ContextSettings(32, 8, 16));
	m_window.setVerticalSyncEnabled(true);
	m_camera.SetPosition(vec3(0, 0, 3500));
}
/*----------------------------------------------------------------------------*/
inline Application::~Application() {
}